<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/assets/css/style.css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css' />
  </head>

  <body>
    <h1>News archive</h1>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/home">Home</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/about">About</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/pages/books">Books</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/news">News</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/news/create">Create News</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/user_authentication/index">Signin</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/user_authentication/signup">Signup</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/user_authentication/logout">Logout</a>
    <a href="https://www.studenti.famnit.upr.si/~klen/xml_part2/codeignitor/index.php/user_authentication/admin">Admin</a>
    
    <xsl:for-each select="news/news_item">
      <h3><xsl:value-of select="title" /></h3> 
      <div class="main"><xsl:value-of select="text" /></div>
      <p><a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/news/{slug}">View article</a></p>    
    </xsl:for-each>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
