<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
	<h1>Books</h1>
	<link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~klen/sis_tmp4/CodeIgnitor/assets/css/style.css" />
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css' />
  </head>

  <body>
	<h1>My Book Collection</h1>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/home">Home</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/about">About</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/pages/books">Books</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/news">News</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/news/create">Create News</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/user_authentication/index">Signin</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/user_authentication/signup">Signup</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/user_authentication/logout">Logout</a>
    	<a href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/index.php/user_authentication/admin">Admin</a>
	<table border="1">
  	<tr bgcolor="#9acd32">
    	<th>Title</th>
    	<th>Auhtor</th>
    	<th>YEar</th>
    	<th>Price</th>
  	</tr>
  	<xsl:for-each select="bookstore/book">
  	<tr>
    	<td><xsl:value-of select="title" /></td>
    	<td>
      	<xsl:for-each select="author"><xsl:value-of select="." /></xsl:for-each>
    	</td>
    	<td><xsl:value-of select="year" /></td>
    	<td><xsl:value-of select="price" /></td>
     	</tr>
   	</xsl:for-each>
	</table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
