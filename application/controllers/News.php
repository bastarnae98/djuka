<?php
class News extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('news_model');
		$this->load->helper('url_helper');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');

		$data['title'] = "Create News";

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/create');
        	$this->load->view('templates/footer');
		}else{
			$this->news_model->set_news();
			$this->load->view('news/success');
		}
	}


	public function edit($slug = NULL){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');

		$data['title'] = "Edit News";

		$data['news_item'] = $this->news_model->get_news_where($slug);

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('news/edit', $data);
        	$this->load->view('templates/footer');
		}else{
			$this->news_model->save_news();
			$this->load->view('news/success');
		}
	}

	public function view($slug)
	{
		
		$data['news_item'] = $this->news_model->get_news_where($slug);
	
		$data['title'] = "News Item";
	

		$this->load->view('templates/header', $data);
        $this->load->view('news/view', $data);
        $this->load->view('templates/footer', $data);

	}


    public function index()
	{
		
		$data['news'] = $this->news_model->get_news();
		$data['title'] = "News Archive";

		header('Content-type: text/xml');
		$xml = new SimpleXMLElement('<news/>');

		$startXML = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="https://www.studenti.famnit.upr.si/~89171035/workspace1/xml2/codeignitor/assets/xslt/news.xsl"?><news></news>';


		$xml = simplexml_load_string($startXML);

		foreach ($data['news'] as $news_item){
			$new_itme_node = $xml->addChild("news_item");
			$fliped = array_flip($news_item);
			array_walk_recursive($fliped, array ($new_itme_node, 'addChild'));    
		}

		print $xml->asXML();
		//die;

		/*$this->load->view('templates/header', $data);
        $this->load->view('news/index', $data);
        $this->load->view('templates/footer', $data);*/

	}

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}


}